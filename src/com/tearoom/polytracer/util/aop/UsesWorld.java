package com.tearoom.polytracer.util.aop;

import com.tearoom.polytracer.model.permanent.World;

/**
 * Created by russelltemplet on 8/4/17.
 */
public interface UsesWorld {

    void initializeWithWorld(World world);
}
