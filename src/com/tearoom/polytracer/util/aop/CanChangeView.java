package com.tearoom.polytracer.util.aop;
;
import com.tearoom.polytracer.main.PolyScene;
import com.tearoom.polytracer.model.permanent.World;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;

/**
 * Created by russelltemplet on 7/29/17.
 */
public interface CanChangeView {

    default void changeView(Event event, String viewName) {
        PolyScene currentScene = (PolyScene) ((Node) event.getSource()).getScene();
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/" + viewName + ".fxml"));
            Parent newRoot = loader.load();
            currentScene.setRoot(newRoot);

            Object controller = loader.getController();
            if (controller instanceof UsesWorld) {
                World world = currentScene.getWorld();
                ((UsesWorld) controller).initializeWithWorld(world);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}