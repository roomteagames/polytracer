package com.tearoom.polytracer.util.aop;

import javafx.event.Event;
import javafx.scene.Node;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Created by russelltemplet on 7/29/17.
 */
public interface CanQuitApp {

    default void requestQuit(Event event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow(); //eww
        stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
    }
}
