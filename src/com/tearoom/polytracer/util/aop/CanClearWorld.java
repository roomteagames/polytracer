package com.tearoom.polytracer.util.aop;

import com.tearoom.polytracer.model.permanent.World;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

public interface CanClearWorld {

    default void attemptClearWorld(World world) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you would like to clear everything in the current world?");
        alert.getDialogPane().getStylesheets().setAll(getClass().getResource("/css/main.css").toExternalForm());
        alert.showAndWait()
                .filter(response -> response == ButtonType.OK)  //TODO: enter on CANCEL needs to NOT cause this to work
                .ifPresent(response -> world.clearShapes());
    }
}