package com.tearoom.polytracer.util;

/**
 * Created by russelltemplet on 7/29/17.
 */
public final class ColorConstants {
    public static final String OLD_POINT_HEX = "#5F9EA0";
    public static final String NEW_POINT_HEX = "#5FD2A0";
    public static final String NOT_ALLOWED = "#A05958";
    public static final String BOUNDING_BOX = "#10AFB6";
    public static final String SHAPE = "#2CB674";
}
