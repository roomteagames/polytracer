package com.tearoom.polytracer.util.struct;

/**
 * Created by russelltemplet on 8/5/17.
 */
public class Pair<T> {

    private T item1;
    private T item2;

    public Pair (T _item1, T _item2) {
        item1 = _item1;
        item2 = _item2;
    }

    public T getItem1() {
        return item1;
    }

    public T getItem2() {
        return item2;
    }
}
