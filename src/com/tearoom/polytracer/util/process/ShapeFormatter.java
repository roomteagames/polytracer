package com.tearoom.polytracer.util.process;

import com.tearoom.polytracer.model.permanent.BoundingBox;
import com.tearoom.polytracer.model.permanent.EdgeFunction;
import com.tearoom.polytracer.model.permanent.EdgePairing;
import com.tearoom.polytracer.model.permanent.Shape;
import com.tearoom.polytracer.model.temporary.PointContainer;
import javafx.geometry.Point2D;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created by russelltemplet on 7/30/17.
 */

//Takes in PointContainer and converts to Shape
public class ShapeFormatter {

    private PointContainer pointContainer;

    public ShapeFormatter (PointContainer _pointContainer) {
        pointContainer = _pointContainer;
    }

    public Shape format() {
        BoundingBox boundingBox = generateBoundingBox();
        EdgePairing edgePairing = generateEdgePairing(boundingBox);

        return new Shape(boundingBox, pointContainer, edgePairing);
    }


    //Internal methods
    private BoundingBox generateBoundingBox() {
        double minX = 9999, maxX = -9999, minY = 9999, maxY = -9999;
        Point2D minPoint = null, maxPoint = null;

        for (Point2D currentPoint : pointContainer.getAllPoints()) {
            if (currentPoint.getX() < minX) {
                minX = currentPoint.getX();
                minPoint = currentPoint;
            } else if (currentPoint.getX() > maxX) {
                maxX = currentPoint.getX();
                maxPoint = currentPoint;
            }

            if (currentPoint.getY() < minY) {
                minY = currentPoint.getY();
            } else if (currentPoint.getY() > maxY) {
                maxY = currentPoint.getY();
            }
        }

        Point2D originPoint = new Point2D(minX, minY);
        int width = (int) (maxX - minX);
        int height = (int) (maxY - minY);

        return new BoundingBox(originPoint, width, height, minPoint, maxPoint);
    }

    private EdgePairing generateEdgePairing(BoundingBox boundingBox) {
        List<EdgeFunction> topFunctions = createEdgeFunctionList(boundingBox, pointContainer.getTopPoints());
        List<EdgeFunction> bottomFunctions = createEdgeFunctionList(boundingBox, pointContainer.getBottomPoints());
        appendLastFunction(boundingBox, topFunctions, bottomFunctions);

        return new EdgePairing(topFunctions, bottomFunctions);
    }

    private List<EdgeFunction> createEdgeFunctionList(BoundingBox boundingBox, List<Point2D> points) {
        List<EdgeFunction> functionList = new ArrayList<>();

        Point2D minPoint = boundingBox.getMinPoint();
        Point2D leftPointRelative = minPoint.subtract(boundingBox.getOrigin());
        Point2D rightPointRelative;

        for (Point2D point : points) {
            if (point != minPoint) {
                rightPointRelative = point.subtract(boundingBox.getOrigin());
                if (rightPointRelative.getX() > leftPointRelative.getX()) { //DO NOT represent vertical lines as functions
                    functionList.add(generateEdgeFunction(leftPointRelative, rightPointRelative));
                }
                leftPointRelative = rightPointRelative;
            }
        }

        return functionList;
    }

    //Creates final function, connecting the last top and last bottom points
    private void appendLastFunction(BoundingBox boundingBox, List<EdgeFunction> topFunctions, List<EdgeFunction> bottomFunctions) {
        Point2D lastTopPoint = pointContainer.getTopPoints().get(pointContainer.getBottomPoints().size() - 1).subtract(boundingBox.getOrigin());
        Point2D lastBottomPoint = pointContainer.getBottomPoints().get(pointContainer.getBottomPoints().size() - 1).subtract(boundingBox.getOrigin());

        if (lastTopPoint.getX() < lastBottomPoint.getX()) {
            topFunctions.add(generateEdgeFunction(lastTopPoint, lastBottomPoint));
        } else if (lastBottomPoint.getX() < lastTopPoint.getX()) {
            bottomFunctions.add(generateEdgeFunction(lastBottomPoint, lastTopPoint));
        }
    }

    private EdgeFunction generateEdgeFunction(Point2D leftPoint, Point2D rightPoint) {
        Point2D diffPoint = rightPoint.subtract(leftPoint);
        double slope = diffPoint.getY() / diffPoint.getX();

        double startX = leftPoint.getX();
        double startY = leftPoint.getY();
        double constant = startY - (startX * slope);

        return new EdgeFunction(slope, constant, (int) startX, (int) rightPoint.getX(), (int) startY, (int) rightPoint.getY());
    }
}
