package com.tearoom.polytracer.panel;

import javafx.scene.Cursor;
import javafx.scene.canvas.Canvas;

/**
 * Created by russelltemplet on 7/29/17.
 */
public class PolyCanvas extends Canvas {

    public PolyCanvas() {
        setWidth(900);
        setHeight(700);
        setCursor(Cursor.CROSSHAIR);
        requestFocus();
    }
}
