package com.tearoom.polytracer.viewcontroller;

import com.tearoom.polytracer.util.aop.CanQuitApp;
import com.tearoom.polytracer.util.aop.CanChangeView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by russelltemplet on 7/28/17.
 */
public class MainMenuController implements Initializable, CanChangeView, CanQuitApp {

    @FXML
    private Button drawModeButton;

    @FXML
    private Button traceModeButton;

    @FXML
    private Button loadButton;

    @FXML
    private Button quitButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    public void onDrawModeClicked(ActionEvent event) {
        changeView(event, "DrawMode");
    }

    @FXML
    public void onTraceModeClicked(ActionEvent event) {
        changeView(event, "TraceMode");
    }

    @FXML
    public void onQuitClicked(ActionEvent event) {
        requestQuit(event);
    }
}
