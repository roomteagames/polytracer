package com.tearoom.polytracer.viewcontroller;

import com.tearoom.polytracer.util.aop.CanChangeView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by russelltemplet on 7/29/17.
 */
public class TraceModeController implements Initializable, CanChangeView {

    @FXML
    private Button drawModeButton;

    @FXML
    private Button mainMenuButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    public void onMainMenuClicked(ActionEvent event) {
        changeView(event, "MainMenu");
    }

    @FXML
    public void onDrawModeClicked(ActionEvent event) {
        changeView(event, "DrawMode");
    }

    @FXML
    public void onKeyPress(KeyEvent event) {
        if (event.getCode() == KeyCode.TAB) {
            changeView(event, "DrawMode");
        } else if (event.getCode() == KeyCode.ESCAPE) {
            changeView(event, "MainMenu");
        }
    }
}
