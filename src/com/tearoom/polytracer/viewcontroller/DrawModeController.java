package com.tearoom.polytracer.viewcontroller;

import com.tearoom.polytracer.model.DrawingModel;
import com.tearoom.polytracer.model.permanent.BoundingBox;
import com.tearoom.polytracer.model.permanent.Shape;
import com.tearoom.polytracer.model.permanent.World;
import com.tearoom.polytracer.model.temporary.DrawingState;
import com.tearoom.polytracer.model.temporary.HoverState;
import com.tearoom.polytracer.model.temporary.PointContainer;
import com.tearoom.polytracer.util.ColorConstants;
import com.tearoom.polytracer.util.aop.CanChangeView;
import com.tearoom.polytracer.util.aop.CanClearWorld;
import com.tearoom.polytracer.util.aop.UsesWorld;
import com.tearoom.polytracer.util.struct.Pair;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by russelltemplet on 7/29/17.
 */
public class DrawModeController implements Initializable, UsesWorld, CanChangeView, CanClearWorld {



    @FXML
    private Button traceModeButton;

    @FXML
    private Button mainMenuButton;

    @FXML
    private Canvas drawCanvas;

    private GraphicsContext gc;

    private DrawingModel drawingModel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        gc = drawCanvas.getGraphicsContext2D();
    }

    @Override
    public void initializeWithWorld(World world) {
        Point2D canvasMidpoint = new Point2D(drawCanvas.getWidth() / 2, drawCanvas.getHeight() / 2);
        drawingModel = new DrawingModel(canvasMidpoint, world);
        redraw();
    }

    @FXML
    public void onMainMenuClicked(ActionEvent event) {
        changeView(event, "MainMenu");
    }

    @FXML
    public void onTraceModeClicked(ActionEvent event) {
        changeView(event, "TraceMode");
    }

    @FXML
    public void onKeyPress(KeyEvent event) {
        switch (event.getCode()) {
            case TAB:
                changeView(event, "TraceMode");
                break;
            case ESCAPE:
                changeView(event, "MainMenu");
                break;
            case ENTER:
                drawingModel.advanceDrawingState();
                break;
            case RIGHT:
                drawingModel.moveRight();
                break;
            case DOWN:
                drawingModel.moveDown();
                break;
            case LEFT:
                drawingModel.moveLeft();
                break;
            case UP:
                drawingModel.moveUp();
                break;
            case N:
                attemptClearWorld(drawingModel.getWorld());
                break;
            case B:
                drawingModel.toggleShowingBoundingBox();
                break;
        }
        redraw();
    }

    @FXML
    public void onCanvasClick(MouseEvent event) {
        drawingModel.addPoint(
                new Point2D(event.getX(), event.getY())
        );
        redraw();
    }

    @FXML
    public void onMouseMove(MouseEvent event) {
        if (drawingModel.updateHoverPoint(
                new Point2D(event.getX(), event.getY())
        )) {
            redraw();
        }
    }


    //internal methods
    private void redraw() {
        gc.clearRect(0, 0, drawCanvas.getWidth(), drawCanvas.getHeight());
        paintCanvasBackground();
        paintShapes();
        paintHoverPoint();
        paintInProgressPoints();
        paintDrawingState();
    }

    private void paintCanvasBackground() {
        gc.setLineWidth(1);
        gc.setStroke(Color.web(ColorConstants.OLD_POINT_HEX, 0.25));
        final int INCREMENT = (int) (DrawingModel.CELL_SIZE * drawingModel.getZoom());

        for (int i = INCREMENT; i < drawCanvas.getHeight(); i+=INCREMENT) {
            gc.strokeLine(0, i, drawCanvas.getWidth(), i);
        }
        for (int j = INCREMENT; j < drawCanvas.getWidth(); j+=INCREMENT) {
            gc.strokeLine(j, 0, j, drawCanvas.getHeight());
        }
    }

    private void paintShapes() {
        drawingModel.getWorld().getShapes().forEach(this::drawShape);
    }

    private void drawShape(Shape shape) {
        if (drawingModel.isShowingBoundingBox()) {
            gc.setFill(Color.web(ColorConstants.NEW_POINT_HEX, 0.25));
            BoundingBox boundingBox = shape.getBoundingBox();
            Point2D originConverted = drawingModel.convertWorldlyToScreenPoint(boundingBox.getOrigin());
            gc.fillRect(originConverted.getX(), originConverted.getY(),
                    boundingBox.getWidth() * DrawingModel.CELL_SIZE, boundingBox.getHeight() * DrawingModel.CELL_SIZE);
        }

        gc.setFill(Color.web(ColorConstants.OLD_POINT_HEX, 0.5));
        Pair<double[]> pointsPair = getShapeXYValues(shape);
        gc.fillPolygon(pointsPair.getItem1(), pointsPair.getItem2(), pointsPair.getItem1().length);
    }

    private Pair<double[]> getShapeXYValues(Shape shape) {
        int numPoints = shape.getPointContainer().getAllPoints().size();
        double[] xValues = new double[numPoints];
        double[] yValues = new double[numPoints];

        int i = 0;
        for (Point2D point : shape.getPointContainer().getTopPoints()) {
            Point2D convertedPoint = drawingModel.convertWorldlyToScreenPoint(point);
            xValues[i] = convertedPoint.getX();
            yValues[i] = convertedPoint.getY();
            i++;
        }
        i = 1;
        for (Point2D point : shape.getPointContainer().getBottomPoints()) {
            Point2D convertedPoint = drawingModel.convertWorldlyToScreenPoint(point);
            xValues[numPoints - i] = convertedPoint.getX();
            yValues[numPoints - i] = convertedPoint.getY();
            i++;
        }

        return new Pair<>(xValues, yValues);
    }

    private void paintHoverPoint() {
        Point2D hoverPoint = drawingModel.getHoverPoint();
        if (hoverPoint != null) {
            hoverPoint = drawingModel.convertWorldlyToScreenPoint(hoverPoint);

            Color color;
            if (drawingModel.getHoverState() == HoverState.ALLOWED) {
                color = Color.web(ColorConstants.NEW_POINT_HEX, 0.5);
            } else {
                color = Color.web(ColorConstants.NOT_ALLOWED, 0.5);
            }

            gc.setStroke(color);
            gc.setFill(color);
            gc.fillOval(hoverPoint.getX() - 5, hoverPoint.getY() - 5, 10, 10);
        }
    }

    private void paintInProgressPoints() {
        PointContainer inProgressContainer = drawingModel.getPointContainer();

        for (int i = 0; i < inProgressContainer.getTopPoints().size(); i++) {
            paintDrawingPoint(inProgressContainer.getTopPoints().get(i), DrawingState.TOP_POINT, i);
        }
        for (int i = 0; i < inProgressContainer.getBottomPoints().size(); i++) {
            paintDrawingPoint(inProgressContainer.getBottomPoints().get(i), DrawingState.BOTTOM_POINT, i);
        }
    }

    private void paintDrawingPoint(Point2D p, DrawingState callingState, int index) {
        Color color = drawingModel.getPointColor(callingState);
        gc.setStroke(color);
        gc.setFill(color);

        Point2D center = drawingModel.convertWorldlyToScreenPoint(p);
        gc.fillOval(center.getX() - 5, center.getY() - 5, 10, 10);

        Point2D lastDrawnPoint = drawingModel.getPreviouslyDrawnPoint(index, callingState);
        if (lastDrawnPoint != null) {
            lastDrawnPoint = drawingModel.convertWorldlyToScreenPoint(lastDrawnPoint);
            gc.setLineWidth(2);
            gc.strokeLine(lastDrawnPoint.getX(), lastDrawnPoint.getY(), center.getX(), center.getY());
        }
    }

    private void paintDrawingState() {
        gc.setFill(Color.web(ColorConstants.OLD_POINT_HEX, 1));
        gc.fillText("[" + drawingModel.getDrawingState().getName() + "]", 10, drawCanvas.getHeight() - 10);
    }
}
