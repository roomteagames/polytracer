package com.tearoom.polytracer.main;

import com.tearoom.polytracer.model.permanent.World;
import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * Created by russelltemplet on 7/29/17.
 */
public class PolyScene extends Scene {

    private World world;

    public PolyScene(Parent root) {
        super(root);

        world = new World();

        loadCSS("main", "canvas");
    }

    private void loadCSS(String... titles) {
        for (String title : titles) {
            String css = getClass().getResource("/css/" + title + ".css").toExternalForm();
            getStylesheets().add(css);
        }
    }

    public void loadWorld() {

    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }
}
