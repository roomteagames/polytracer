package com.tearoom.polytracer.main;

import com.tearoom.polytracer.main.PolyScene;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.util.Optional;

/**
 * Created by russelltemplet on 7/28/17.
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/view/MainMenu.fxml"));
        Scene scene = new PolyScene(root);

        primaryStage.setScene(scene);
        primaryStage.setResizable(false);

        primaryStage.setOnCloseRequest(e -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you would like to quit? Unsaved changes will be lost!");
            alert.getDialogPane().getStylesheets().setAll(getClass().getResource("/css/main.css").toExternalForm());  //TODO: put this code somewhere better
            alert.showAndWait()
                    .filter(response -> response == ButtonType.CANCEL)
                    .ifPresent(response -> e.consume());
        });
        primaryStage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
