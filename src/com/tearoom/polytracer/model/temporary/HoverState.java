package com.tearoom.polytracer.model.temporary;

/**
 * Created by russelltemplet on 7/29/17.
 */
public enum HoverState {
    ALLOWED,
    NOT_ALLOWED
}
