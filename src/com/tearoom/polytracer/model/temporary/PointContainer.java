package com.tearoom.polytracer.model.temporary;

import com.tearoom.polytracer.model.modelview.Point;
import javafx.geometry.Point2D;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by russelltemplet on 7/29/17.
 */
public class PointContainer {

    private List<Point2D> topPoints;
    private List<Point2D> bottomPoints;
    private List<List<Point2D>> concavityPoints;
    private List<Point2D> allPoints;

    public PointContainer () {
        topPoints = new ArrayList<>();
        bottomPoints = new ArrayList<>();
        concavityPoints = new ArrayList<>();
        allPoints = new ArrayList<>();
    }

    public List<Point2D> getTopPoints() {
        return topPoints;
    }

    public List<Point2D> getBottomPoints() {
        return bottomPoints;
    }

    public List<List<Point2D>> getConcavityPoints() {
        return concavityPoints;
    }

    public List<Point2D> getAllPoints() {
        if (allPoints.size() != (topPoints.size() + bottomPoints.size())) {
            allPoints.clear();
            allPoints.addAll(topPoints);
            allPoints.addAll(bottomPoints);
        }
        return allPoints;
    }
}
