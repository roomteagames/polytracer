package com.tearoom.polytracer.model.temporary;

/**
 * Created by russelltemplet on 7/29/17.
 */
public enum DrawingState {

    IDLE("Idle"),
    TOP_POINT("Top Points"),
    BOTTOM_POINT("Bottom Points"),
    CONCAVE_TOP_POINT("Concave Top Points"),
    CONCAVE_BOTTOM_POINT("Concave Bottom Points");

    private String name;

    DrawingState(String _name) {
        name = _name;
    }

    public String getName() {
        return name;
    }
}
