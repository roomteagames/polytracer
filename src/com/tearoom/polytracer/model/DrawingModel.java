package com.tearoom.polytracer.model;

import com.tearoom.polytracer.model.permanent.Shape;
import com.tearoom.polytracer.model.permanent.World;
import com.tearoom.polytracer.model.temporary.DrawingState;
import com.tearoom.polytracer.model.temporary.HoverState;
import com.tearoom.polytracer.model.temporary.PointContainer;
import com.tearoom.polytracer.util.ColorConstants;
import com.tearoom.polytracer.util.process.ShapeFormatter;
import javafx.geometry.Point2D;
import javafx.scene.paint.Color;

/**
 * Created by russelltemplet on 7/29/17.
 */
public class DrawingModel {

    public static final int CELL_SIZE = 25;

    private HoverState hoverState;
    private DrawingState drawingState;

    private Point2D canvasCenter;
    private Point2D worldOffset;
    private Point2D hoverPoint;

    private PointContainer pointContainer;
    private World world;

    private boolean isShowingBoundingBox;

    private double zoom;

    public DrawingModel (Point2D _canvasCenter, World _world) {
        canvasCenter = _canvasCenter;
        world = _world;
        worldOffset = new Point2D(0,0);

        drawingState = DrawingState.IDLE;
        hoverState = HoverState.ALLOWED;
        zoom = 1;

        isShowingBoundingBox = true;

        flushPointContainer();
    }

    public void addPoint(Point2D clickPoint) {
        if (hoverState == HoverState.ALLOWED) {
            Point2D clickPointConverted = convertScreenToWorldlyPoint(clickPoint);

            switch (drawingState) {
                case IDLE:
                    drawingState = DrawingState.TOP_POINT;
                case TOP_POINT:
                    pointContainer.getTopPoints().add(clickPointConverted);
                    break;
                case BOTTOM_POINT:
                    pointContainer.getBottomPoints().add(clickPointConverted);
                    break;

            }
            hoverState = HoverState.NOT_ALLOWED;
        }
    }

    public boolean updateHoverPoint(Point2D mousePoint) {
        boolean updated = false;
        Point2D newHoverPoint = convertScreenToWorldlyPoint(mousePoint);

        if (!newHoverPoint.equals(hoverPoint)) {
            hoverPoint = newHoverPoint;
            updateHoverState();
            updated = true;
        }
        return updated;
    }

    public void moveRight() {
        moveWorldOffset(1, 0);
    }

    public void moveDown() {
        moveWorldOffset(0, 1);
    }

    public void moveLeft() {
        moveWorldOffset(-1, 0);
    }

    public void moveUp() {
        moveWorldOffset(0, -1);
    }

    public void advanceDrawingState() {
        switch (drawingState) {
            case TOP_POINT:
                drawingState = DrawingState.BOTTOM_POINT;
                break;
            case BOTTOM_POINT:
                Shape shape = new ShapeFormatter(pointContainer).format();
                world.getShapes().add(shape);
                drawingState = DrawingState.IDLE;
                flushPointContainer();
                break;
        }
    }

    public Point2D convertScreenToWorldlyPoint(Point2D screenPoint) {
        Point2D precisePoint = screenPoint
                .subtract(canvasCenter)
                .multiply(1/(CELL_SIZE * zoom))
                .add(worldOffset);

        return new Point2D(
                Math.round(precisePoint.getX()),
                Math.round(precisePoint.getY())
        );
    }

    public Point2D convertWorldlyToScreenPoint(Point2D worldlyPoint) {
        return worldlyPoint
                .subtract(worldOffset)
                .multiply(CELL_SIZE * zoom)
                .add(canvasCenter);
    }

    public Point2D getPreviouslyDrawnPoint(int index, DrawingState callingState) {
        Point2D previousPoint = null;
        if (callingState == DrawingState.TOP_POINT && index > 0) {
            previousPoint = pointContainer.getTopPoints().get(index - 1);
        } else if (callingState == DrawingState.BOTTOM_POINT && index == 0) {
            previousPoint = pointContainer.getTopPoints().get(0);
        } else if (callingState == DrawingState.BOTTOM_POINT && index > 0) {
            previousPoint = pointContainer.getBottomPoints().get(index - 1);
        }

        return previousPoint;
    }

    public Point2D getPreviouslyAddedPoint() {
        Point2D previousPoint = null;
        if (drawingState == DrawingState.TOP_POINT && pointContainer.getTopPoints().size() > 0) {
            previousPoint = pointContainer.getTopPoints().get(
                    pointContainer.getTopPoints().size() - 1
            );
        } else if (drawingState == DrawingState.BOTTOM_POINT && pointContainer.getBottomPoints().size() > 0) {
            previousPoint = pointContainer.getBottomPoints().get(
                    pointContainer.getBottomPoints().size() - 1
            );
        }
        return previousPoint;
    }

    public Color getPointColor(DrawingState callingState) {
        String hex = ColorConstants.OLD_POINT_HEX;
        if (callingState == drawingState) {
            hex = ColorConstants.NEW_POINT_HEX;
        }
        return Color.web(hex, 1);
    }


    //internal / helpers
    private void moveWorldOffset (int xAmount, int yAmount) {
        worldOffset = worldOffset.add(new Point2D(xAmount, yAmount));
    }

    private void flushPointContainer() {
        pointContainer = new PointContainer();
    }

    private void updateHoverState() {
        if (getPreviouslyAddedPoint() == null || hoverPoint.getX() > getPreviouslyAddedPoint().getX()) {
            hoverState = HoverState.ALLOWED;
        } else {
            hoverState = HoverState.NOT_ALLOWED;
        }

        if (drawingState == DrawingState.BOTTOM_POINT) {
            //TODO: lots more validation to ensure point isn't ever above an upper line
        }
    }


    //getters


    public HoverState getHoverState() {
        return hoverState;
    }

    public DrawingState getDrawingState() {
        return drawingState;
    }

    public double getZoom() {
        return zoom;
    }

    public PointContainer getPointContainer() {
        return pointContainer;
    }

    public World getWorld() {
        return world;
    }

    public Point2D getHoverPoint() {
        return hoverPoint;
    }

    public boolean isShowingBoundingBox() {
        return isShowingBoundingBox;
    }

    public void toggleShowingBoundingBox() {
        isShowingBoundingBox = !isShowingBoundingBox;
    }
}
