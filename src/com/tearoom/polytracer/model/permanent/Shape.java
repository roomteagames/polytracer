package com.tearoom.polytracer.model.permanent;

import com.tearoom.polytracer.model.temporary.PointContainer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by russelltemplet on 7/29/17.
 */
public class Shape implements Serializable {

    private BoundingBox boundingBox;
    private EdgePairing edgePairing;
    private PointContainer pointContainer;
    private List<EdgePairing> concavityPairings;

    public Shape (BoundingBox _boundingBox, PointContainer _pointContainer, EdgePairing _edgePairing) {
        boundingBox = _boundingBox;
        pointContainer = _pointContainer;
        edgePairing = _edgePairing;
        concavityPairings = new ArrayList<>();
    }

    public BoundingBox getBoundingBox() {
        return boundingBox;
    }

    public EdgePairing getEdgePairing() {
        return edgePairing;
    }

    public List<EdgePairing> getConcavityPairings() {
        return concavityPairings;
    }

    public PointContainer getPointContainer() {
        return pointContainer;
    }
}
