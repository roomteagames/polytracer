package com.tearoom.polytracer.model.permanent;

import java.io.Serializable;
import java.util.List;

/**
 * Created by russelltemplet on 7/29/17.
 */
public class EdgePairing implements Serializable {

    private List<EdgeFunction> topFunctions;
    private List<EdgeFunction> bottomFunctions;

    public EdgePairing (List<EdgeFunction> _topFunctions, List<EdgeFunction> _bottomFunctions) {
        topFunctions = _topFunctions;
        bottomFunctions = _bottomFunctions;
    }

    public List<EdgeFunction> getTopFunctions() {
        return topFunctions;
    }

    public List<EdgeFunction> getBottomFunctions() {
        return bottomFunctions;
    }
}
