package com.tearoom.polytracer.model.permanent;

import javafx.geometry.Point2D;

/**
 * Created by russelltemplet on 7/29/17.
 */
public class BoundingBox {

    private Point2D origin;
    private Point2D minPoint;
    private Point2D maxPoint;

    private int width;
    private int height;

    public BoundingBox (Point2D _origin, int _width, int _height, Point2D _minPoint, Point2D _maxPoint) {
        origin = _origin;
        width = _width;
        height = _height;

        minPoint = _minPoint;
        maxPoint = _maxPoint;
    }

    public Point2D getOrigin() {
        return origin;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Point2D getMinPoint() {
        return minPoint;
    }

    public Point2D getMaxPoint() {
        return maxPoint;
    }
}
