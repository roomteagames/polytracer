package com.tearoom.polytracer.model.permanent;

import javafx.geometry.Point2D;

import java.io.Serializable;

/**
 * Created by russelltemplet on 7/29/17.
 */
public class Edge implements Serializable {

    private Point2D point1;
    private Point2D point2;
    private EdgeFunction function;

    public Edge (Point2D _point1, Point2D _point2, EdgeFunction _function) {
        point1 = _point1;
        point2 = _point2;
        function = _function;
    }

    public Point2D getPoint1() {
        return point1;
    }

    public Point2D getPoint2() {
        return point2;
    }

    public EdgeFunction getFunction() {
        return function;
    }
}
