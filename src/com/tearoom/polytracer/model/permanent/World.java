package com.tearoom.polytracer.model.permanent;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by russelltemplet on 7/29/17.
 */
public class World implements Serializable {

    private String name;
    private List<Shape> shapes; //TODO: eventually try converting this to a grid

    public World() {
        clearShapes();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Shape> getShapes() {
        return shapes;
    }

    public void clearShapes() {
        shapes = new ArrayList<>();
    }
}
