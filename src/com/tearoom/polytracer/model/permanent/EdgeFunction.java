package com.tearoom.polytracer.model.permanent;

import java.io.Serializable;

/**
 * Created by russelltemplet on 7/29/17.
 */
public class EdgeFunction implements Serializable {

    private double quadraticCoefficient;
    private double linearCoefficient;
    private double constant;
    private int startX;
    private int endX;
    private int startY;
    private int endY;

    public EdgeFunction (double _quadraticCoefficient, double _linearCoefficient, double _constant, int _startX, int _endX, int _startY, int _endY) {
        quadraticCoefficient = _quadraticCoefficient;
        linearCoefficient = _linearCoefficient;
        constant = _constant;
        startX = _startX;
        endX = _endX;
        startY = _startY;
        endY = _endY;
    }

    public EdgeFunction (double _linearCoefficient, double _constant, int _startX, int _endX, int _startY, int _endY) {
        this(0, _linearCoefficient, _constant, _startX, _endX, _startY, _endY);
    }

    public double getQuadraticCoefficient() {
        return quadraticCoefficient;
    }

    public double getLinearCoefficient() {
        return linearCoefficient;
    }

    public double getConstant() {
        return constant;
    }

    public int getStartX() {
        return startX;
    }

    public int getEndX() {
        return endX;
    }

    public int getStartY() {
        return startY;
    }

    public int getEndY() {
        return endY;
    }
}
